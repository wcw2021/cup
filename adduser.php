<?php
session_start();

//Open a new connection to the MySQL server
$conn = new mysqli('localhost', 'noname', 'nopass', 'perfectcup');

//Output any connection error
if ($conn->connect_error) {
    die('Error : (' . $conn->connect_errno . ') ' . $conn->connect_error);
}

$fname = $conn->real_escape_string($_POST['fname']);
$lname = $conn->real_escape_string($_POST['lname']);
$email = $conn->real_escape_string($_POST['email']);
$password = $conn->real_escape_string($_POST['password']);

//VALIDATION

if (strlen($fname) < 1) {
    echo 'fname-length-error';
} elseif (strlen($lname) < 1) {
    echo 'lname-length-error';
} elseif (strlen($email) < 1) {
    echo 'email-length-error';
} elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
    echo 'email-format-error';
} elseif (strlen($password) <= 4) {
    echo 'password-length-error';	
} else {
	
	//PASSWORD ENCRYPT
	$hash_password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));

    $sql = "SELECT * FROM members WHERE email='$email'";
    $result = $conn->query($sql);
    $num_row = $result->num_rows;
    $row = $result->fetch_assoc();
	
    if ($num_row < 1) {
        $stmt = $conn->prepare("INSERT INTO members (fname, lname, email, password) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("ssss", $fname, $lname, $email, $hash_password);
        $stmt->execute();

        if ($stmt) {

            $_SESSION['login'] = $conn->insert_id;
            $_SESSION['fname'] = $fname;
            $_SESSION['lname'] = $lname;

            echo 'true';
        }

        $stmt->close();

    } else {
        echo 'false';
    }
		
}

$conn->close();


