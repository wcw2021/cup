<?php
session_start();

$email = htmlspecialchars($_POST['email']);
$password = htmlspecialchars($_POST['password']);

//Open a new connection to the MySQL server
$conn = new mysqli('localhost', 'noname', 'nopass', 'perfectcup');


//Output any connection error
if ($conn->connect_error) {
    die('Error : (' . $conn->connect_errno . ') ' . $conn->connect_error);
}

$sql = "SELECT * FROM members WHERE email='$email'";
$result = $conn->query($sql);
$num_row = $result->num_rows;
$row = $result->fetch_assoc();

if ($num_row == 1) {

    if (password_verify($password, $row['password'])) {

        $_SESSION['login'] = $row['id'];
        $_SESSION['fname'] = $row['fname'];
        $_SESSION['lname'] = $row['lname'];
        echo 'true';
    }
    else {
        echo 'false';
    }

} else {
    echo 'false';
}

$conn->close();


